import validate from './validate.js'

window.addEventListener('DOMContentLoaded', _ => {

    const form = document.getElementById('add-link-form')

    form.addEventListener('submit', e => {
        if (validate()) {
            alert("FORM IS VALID!");
        }
    })

})