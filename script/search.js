import mock from '../public/data/mockData.js'
import paginate from './pagination.js'

window.addEventListener('DOMContentLoaded', _ => {
    const params = new URL(window.location.href).searchParams;
    let keyword = params.get('keyword') || ''
    let page = parseInt(params.get('page')) || 1
    const sort = params.get('sort') || 'ascending'

    const inputGroup = document.querySelector('.search-header')
    const results = document.querySelector('.results')
    const input = inputGroup.querySelector('input')

    let debounceRef
    input.value = keyword
    input.addEventListener('keydown', e => {
        clearTimeout(debounceRef)
        debounceRef = setTimeout(() => {
            keyword = e.target.value
            inputGroup.querySelector('.search-link').setAttribute('href', `/search.html?keyword=${keyword}`)
            page = 1
            searchResults(keyword, page, sort)
            setSortItems()
        }, 500)
    })

    function setSortItems() {
        document.querySelectorAll('.sort-item').forEach(a => {
            const type = a.dataset.sorttype
            console.log(type);
            a.setAttribute('href', `/search.html?keyword=${keyword}&page=${page}&sort=${type}`)

        })
    }

    function searchResults(keyword, page, sort) {
        const limit = 5

        const data = mock.filter(y => y['Name Surname']
            .includes(keyword.toLowerCase())).sort((a, b) => {
                if (sort === 'ascending') {
                    if (a['Name Surname'] < b['Name Surname']) {
                        return -1
                    } else {
                        return 1
                    }
                }
                if (sort === 'descending') {
                    if (a['Name Surname'] < b['Name Surname']) {
                        return 1
                    } else {
                        return -1
                    }
                }
                if (sort === 'date-ascending') {
                    if (Date.parse(a['Date']) < Date.parse(b['Date'])) {
                        return -1
                    } else {
                        return 1
                    }
                }
                if (sort === 'date-descending') {
                    if (Date.parse(a['Date']) < Date.parse(b['Date'])) {
                        return 1
                    } else {
                        return -1
                    }
                }
            })


        const currentPage = (page - 1) * limit
        results.querySelector('.results-base').innerHTML = data.slice(currentPage, page * limit)
            .map(item => {
                return `
                    <a href="/search.html?keyword=${item['Name Surname']}" class="flex wrap result-parent">
                        <div class="flex list-tile" style="--spacing: 1rem">
                            <img src="./public/icons/location.svg" alt="Location icon" aria-hidden="true" />
                            <div>
                                <p class="title color darkest">${item['Name Surname']}</p>
                                <small class="subtitle color lighter">${item['Country']}, ${item['City']}</small>
                            </div>
                        </div>
                        <div class="trailing">
                            <p class="color-gray text normal" style="--fontSize: 16px; --font-family: 'Inter';">${item['Name Surname']}</p>
                            <p class="color-gray text semibold" style="--font-family: 'Inter';">${item['Date']}</p>
                        </div>
                    </a>
                    <div class="divider"></div>
                `
            }).join('')


        paginate(Math.ceil(data.length / limit), 3, page, keyword, sort)
    }


    searchResults(keyword, page, sort)
    setSortItems()
})
