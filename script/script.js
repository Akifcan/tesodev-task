import mock from '../public/data/mockData.js'
window.addEventListener('DOMContentLoaded', _ => {


    const arrowLeft = document.querySelector('.arrow-left')
    const arrowRight = document.querySelector('.arrow-right')
    const carousel = document.querySelector('.carousel')
    const searchResults = document.querySelector('.search-results')
    const inputGroup = document.querySelector('.input-group')


    let debounceRef

    inputGroup.querySelector('input').addEventListener('keydown', e => {
        clearTimeout(debounceRef)
        debounceRef = setTimeout(() => {

            if (e.target.value.length === 0) {
                searchResults.classList.add('none')
                searchResults.innerHTML = ''
                return
            }

            inputGroup.querySelector('a').setAttribute('href', `/search.html?keyword=${e.target.value}`)

            const result = mock.filter(y => y['Name Surname'].includes(e.target.value.toLowerCase())).slice(0, 3).map(item => {
                return `
                    <a href="/search.html?keyword=${item['Name Surname']}">
                        <div class="flex list-tile" style="--spacing: 1rem">
                            <img src="./public/icons/location.svg" alt="Location icon" aria-hidden="true" />
                            <div>
                                <p class="title color darkest">${item['Name Surname']}</p>
                                <small class="subtitle color lighter">${item['Country']}, ${item['City']}</small>
                            </div>
                        </div>
                    </a>
                `
            }).join('')

            const showMore = `<a style="align-self: center;" href="/search.html?keyword=${e.target.value}"><button class="show-more color darkest button ghost" style="--font-family: 'Inter';">Show
                        more...</button></a>`

            if (result) {
                searchResults.classList.remove('none')
                searchResults.innerHTML = result.concat(showMore)

            } else {
                searchResults.classList.add('none')
                searchResults.innerHTML = ''
            }
        }, 500);
    })

    function runCarousel() {
        if (carousel.scrollLeft === 0) {
            arrowLeft.setAttribute('hidden', 'hidden')
        } else {
            arrowLeft.removeAttribute('hidden')
        }
        const { scrollWidth, scrollLeft, clientWidth } = carousel;
        const distanceFromLeft = scrollWidth - scrollLeft - clientWidth;
        if (distanceFromLeft < 20) {
            arrowRight.setAttribute('hidden', 'hidden')
        } else {
            arrowRight.removeAttribute('hidden')
        }
    }

    carousel.addEventListener('scroll', runCarousel)

    arrowRight.addEventListener('click', _ => {
        const boxWidth = document.querySelector('.carousel-item').clientWidth
        carousel.scrollTo({
            left: carousel.scrollLeft + boxWidth + 16, behavior: 'smooth'
        })
        runCarousel()
    })

    arrowLeft.addEventListener('click', _ => {
        const boxWidth = document.querySelector('.carousel-item').clientWidth
        carousel.scrollTo({
            left: carousel.scrollLeft - boxWidth - 16, behavior: 'smooth'
        })
        runCarousel()
    })

    runCarousel()

})