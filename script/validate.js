const showErrors = (messages, base) => {

    const ul = document.createElement('ul')
    ul.className = 'my-small error-ul'
    ul.innerHTML = messages.map(message => `<li class="text bold color">${message + '. '}</li>`).join('')

    base.appendChild(ul)

}

const showAlert = () => {

    if (document.querySelector('.alert')) {
        document.querySelector('.alert').remove()
    }

    const messages = [...document.querySelectorAll('.not-valid')]
    const alertMessages = messages.map(message => {
        return `<b>${message.querySelector('p').textContent}:</b>  ${[...message.querySelectorAll('ul')].map(e => {
            return e.textContent
        })}`
    })
    const alert = document.createElement('div')
    alert.className = 'alert bottom-right active'
    alert.innerHTML = `
        <div>
            <p class="text bold">Error while adding link element</p>
            <ul id="alert-ul">
                ${alertMessages.map(message => `<li class="text normal color">${message}</li>`).join('')}
            </ul>
        </div>
        <div class="close" aria-label="Click for Close Alert Dialog" id="remove-alert">
            <img src="./public/icons/close.svg" alt="close" />
        </div>
        <button class="button danger">Error</button>
    `

    document.body.appendChild(alert)

    document.getElementById('remove-alert').addEventListener('click', _ => {
        alert.classList.add('alert-leave')
        alert.addEventListener('animationend', () => {
            alert.remove()
        })
    })


}

function validate() {
    const inputs = document.querySelectorAll(`[data-validate]`)

    inputs.forEach(input => {
        input.classList.remove('not-valid')
    })

    inputs.forEach(input => {

        const errorMessages = []
        const validations = input.dataset.validate.split('|')
        const base = input.parentElement
        const value = input.value


        validations.forEach(e => {
            if (e === 'required') {
                console.log("required");
                if (value.length <= 0) {
                    errorMessages.push('Please fill this area')
                }
            }
            if (e === 'email') {
                console.log("email");
                if (!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,20}$/.test(value)) {
                    errorMessages.push('Your email address format is wrong')
                }
            }
            if (e.includes('min')) {
                console.log("min");
                const min = parseInt(e.split('-')[1])
                if (value.length < min) {
                    errorMessages.push(`Please enter at least ${min} character`)
                }
            }
            if (e.includes('max')) {
                console.log("min");
                const max = parseInt(e.split('-')[1])
                if (value.length > max) {
                    errorMessages.push(`Please enter max ${min} character`)
                }
            }

        })

        if (base.querySelector('ul')) {
            base.removeChild(base.querySelector('ul'));
        }
        if (errorMessages.length) {
            base.classList.add('not-valid')
            base.setAttribute('aria-valid', 'false')
            showErrors(errorMessages, base)
        } else {
            base.setAttribute('aria-valid', 'true')
            base.classList.remove('not-valid')
        }

    })
    const isValid = [...document.querySelectorAll('.not-valid')].length === 0
    if (!isValid) showAlert()
    return isValid

}

export default validate