const paginationBase = document.querySelector('.pagination')

const paginate = (totalPage, show, currentPage, keyword, order) => {
    const pageArr = []
    const next = []
    const prev = []
    for (let i = 0; i <= totalPage; i++) {
        pageArr.push(i)
    }
    for (let i = 0; i < show; i++) {
        if (pageArr[i + currentPage + 1]) {
            next.push(pageArr[i + currentPage + 1])
        }
    }
    for (let i = show; i > 0; i--) {
        if (pageArr[currentPage - i]) {
            prev.push(currentPage - i)
        }
    }

    if (totalPage > 1) {


        paginationBase.innerHTML = ''

        const previousButton = document.createElement('div')
        previousButton.className = 'button text semibold paginate-button'
        previousButton.innerText = 'Previous'
        previousButton.addEventListener('click', _ => {
            if (currentPage !== 1) {
                window.location.href = `/search.html?keyword=${keyword}&page=${currentPage - 1}&sort=${order}`
            }
        })

        paginationBase.appendChild(previousButton)

        for (let i = show; i > 0; i--) {
            if (pageArr[currentPage - i]) {
                const page = pageArr[currentPage - i]
                const div = document.createElement('div')
                div.innerText = page
                div.className = 'button  text semibold paginate-button'
                div.addEventListener('click', _ => {
                    window.location.href = `/search.html?keyword=${keyword}&page=${page}&sort=${order}`
                })
                paginationBase.appendChild(div)
            }
        }

        const current = document.createElement('div')
        current.innerText = currentPage
        current.className = 'button  text semibold paginate-button active'
        current.addEventListener('click', _ => {
            window.location.href = `/search.html?keyword=${keyword}&page=${currentPage}&sort=${order}`
        })

        paginationBase.appendChild(current)


        for (let i = 0; i < show; i++) {
            if (pageArr[i + currentPage + 1]) {
                const page = pageArr[i + currentPage + 1]
                const div = document.createElement('div')
                div.innerText = page
                div.className = 'button  text semibold paginate-button'
                div.addEventListener('click', _ => {
                    window.location.href = `/search.html?keyword=${keyword}&page=${page}&sort=${order}`
                })
                paginationBase.appendChild(div)
            }
        }



        const nextButton = document.createElement('div')
        nextButton.className = 'button  text semibold paginate-button'
        nextButton.innerText = 'Next'
        nextButton.addEventListener('click', _ => {
            if (currentPage !== totalPage) {
                window.location.href = `/search.html?keyword=${keyword}&page=${currentPage + 1}&sort=${order}`
            }
        })
        paginationBase.appendChild(nextButton)
    }

}


export default paginate